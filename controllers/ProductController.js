const Product = require('../models/Product');

// Admin User - Product creation
module.exports.addProduct = (data) => {
    if(data.isAdmin){
        let new_product = new Product({
            name: data.product.name,
            description: data.product.description,
            price: data.product.price
        });
        return new_product.save().then((new_product, error) => {
            if(error){
                return false;
            };
            return {
                message: 'Successfully added new product!'
            };
        });
    };
    let message = Promise.resolve({
        message: 'User must be ADMIN to access this feature.'
    });
    return message.then((value) => {
        return value;
    });
};

// Get all Products
module.exports.getAllProducts = () => {
    return Product.find().then((result) => {
        return result
    })
}

// Get all Active product(s)
module.exports.getAllActive = () => {
    return Product.find({isActive: true}).then((result) => {
        return result;
    });
};

// Retrieve single product
module.exports.getProduct = (productId) => {
    return Product.findById(productId).then((result) => {
        return result;
    });
};

// Update product
module.exports.updateProduct = (product_id, data) => {
    if(data.isAdmin){
        return Product.findByIdAndUpdate(product_id, {
            name: data.product.name,
            description: data.product.description,
            price: data.product.price
        }).then((product_update, error) => {
            if(error){
                return false;
            };
            return {
                message: 'Product successfully updated!'
            };
        });
    };
    let message = Promise.resolve({
        message: 'User must be ADMIN to access this feature.'
    });
    return message.then((value) => {
        return value;
    });
};

// Archive product
module.exports.archiveProduct = (product_id, data) => {
    if(data.isAdmin){
        return Product.findByIdAndUpdate(product_id, {
            isActive: false
        }).then((product_update, error) => {
            if(error){
                return false;
            };
            return {
                message: 'Product successfully moved to archive!'
            };
        });
    };
    let message = Promise.resolve({
        message: 'User must be ADMIN to access this feature.'
    });
    return message.then((value) => {
        return value;
    });
};

// Retrieve product
module.exports.retrieveProduct = (product_id, data) => {
    if(data.isAdmin){
        return Product.findByIdAndUpdate(product_id, {
            isActive: true
        }).then((product_update, error) => {
            if(error){
                return false;
            };
            return {
                message: 'Product successfully retrieved!'
            };
        });
    };
    let message = Promise.resolve({
        message: 'User must be ADMIN to access this feature.'
    });
    return message.then((value) => {
        return value;
    });
};