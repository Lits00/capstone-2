const User = require('../models/User');
const Product = require('../models/Product')
const Order = require('../models/Order');
const Cart = require('../models/Cart')
const bcrypt = require('bcrypt');
const auth = require('../auth');

// User Registration
module.exports.register = (data) => {
    return User.find({email: data.email}).then((result) =>{
        if(result.length > 0){
            return false
            // return {
            //     message: "User already exist."
            // }
        }
        // For password encryption
        let password_encryption = bcrypt.hashSync(data.password, 10);
        
        let new_user = new User({
            email: data.email,
            password: password_encryption,
            mobileNo: data.mobileNo
        });
        return new_user.save().then((registered_user, error) => {
            if(error){
                return false
            };
            return {
                message: 'User successfully registered!'
            };
        });
    })
};

// Login User Authentication
module.exports.login = (data) => {
    return User.findOne({email: data.email}).then((result) => {
        if(result == null){
            return {
                message: "User doesn't exist!"
            }
        };

        const compare_password = bcrypt.compareSync(data.password, result.password)
        if(compare_password){
            return {
                accessToken: auth.createAccessToken(result)
            }
        };
        return {
            message: 'Incorrect Password!'
        };
    })
};

// Get single user based on ID from token
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Makes the password not be included in the result
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});
};

// User Checkout (Create Order)
module.exports.orderProduct = async (data) => {
    if(data.isAdmin){
        let message = Promise.resolve({
            message: "Admin users can't access this feature."
        })
        return message.then((result) => result)
    }
    // Responsible for storing product info to be extracted for total computation
    let productInfo = await Product.findById(data.info.productId).then((productDetails) => productDetails)
    let new_order = new Order({
        userId: data.info.userId,
        products: {
            productId: data.info.productId,
            name: data.info.name,
            quantity: data.info.quantity
        },
        totalAmount: productInfo.price*data.info.quantity
    })
    return new_order.save().then((order, error) => {
        if(error){
            return false
        }
        return {
            message: 'Order successful!'
        }
    })
};

// Retrieve user details
module.exports.getUserDetails = (user_id) => {
    return User.findById(user_id, {password: 0}).then((result) => result)
}

// [Stretch Goal]
// Set user as admin (Admin Only)
module.exports.setAsAdmin = (userId, data) => {
    if(data.isAdmin){
        return User.findByIdAndUpdate(userId, {
            isAdmin: true
        }).then((product_update, error) => {
            if(error){
                return false;
            };
            return {
                message: 'User was set as Admin'
            };
        });
    }
    let message = Promise.resolve({
        message: 'User must be ADMIN to access this feature.'
    });
    return message.then((value) => value);
}

// Retrieve non-admin orders
module.exports.getUserOrders = async (userId, data) => {
    // Checks if user exist or if userId is incorrect
    let id = await User.findById(data.userId).then((result) => {
        if(result == null){
            return false
        }
        return result._id
    })
    if(userId == id){
        return Order.find({userId: userId}).then((result) => result)
    }
    return {
        message: "Error! Incorrect userID or the user you're trying to access doesn't exist!"
    }
}

// Retrieve user orders
module.exports.getUserOrders = async (userId, data) => {
    if(data.isAdmin){
        let message = Promise.resolve({
            message: "Admin users can't access this feature."
        })
        return message.then((result) => result)
    }
    // Checks if user exist or if userId is incorrect
    let id = await User.findById(data.userId).then((result) => {
        if(result == null){
            return false
        }
        return result._id
    })
    if(userId == id){
        return Order.find({userId: userId}).then((result) => result)
    }
    return {
        message: "Error! Incorrect userID or the user you're trying to access doesn't exist!"
    }
}

// Retrieve all orders (Admin)
module.exports.getAllOrders = (data) => {
    if(data.isAdmin){
        return Order.find().then((result) => result)
    }
    return Promise.resolve({
        message: 'User must be ADMIN to access this feature.'
    })
}

// Add to Cart
module.exports.addToCart = (data) => {
    if(data.isAdmin){
        return Promise.resolve({
            message: "Admin users can't access this feature."
        })
    }
    let new_cart = new Cart({
        userId: data.info.userId
    })
    return new_cart.save().then((cart, error) => {
        if(error){
            return false
        }
        return {
            message: "Initializing cart."
        }
    })
}

module.exports.addItems = async (data) => {
    if(data.isAdmin){
        return Promise.resolve({
            message: "Admin users can't access this feature."
        })
    }
    let productInfo = await Product.findById(data.info.productId).then((productDetails) => productDetails)
    let cartProducts_updated = await Cart.findOne({userId: data.info.userId}).then((cart) => {
        cart.products.push({
            productId: productInfo.productId,
            name: productInfo.name,
            price: productInfo.price,
            quantity: data.quantity,
            subtotal: productInfo.price*data.info.quantity
        })
        return cart.save().then((updated_cart, error) => {
            if(error){
                return false
            }
            return true
        })
    })
    // get product data-info
    if(cartProducts_updated){
        let sum

        Cart.findOne({userId: data.info.userId}).then((result) =>{
            sum = result.products.reduce((total, value) => {
                return total + value.subtotal
            }, 0)
        })

        Cart.findOneAndUpdate({userId: data.info.userId}, {
          total: sum
        }).then((cart) => {
            return {
                message: 'Order Successful!'
            }
        })
        
    }
}