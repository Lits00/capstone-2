const jwt = require('jsonwebtoken');

const secret = "CapstoneEcommersAPI";

// Creates security key
module.exports.createAccessToken = (user) => {
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    };
    return jwt.sign(data, secret, {});
};

// User verification
module.exports.verify = (request, response, next) => {
    let key = request.headers.authorization;

    if(typeof key !== "undefined"){
        
        key = key.slice(7, key.length);

        return jwt.verify(key, secret, (error, data) => {
            if(error){
                return response.send({
                    auth: "Failed."
                });
            } else {
                next()
            };
        });
    } else {
        return response.send({
            auth: "Failed."
        })
    };
};

// Responsible for extracting specific user data that we need
module.exports.decode = (token) => {
    if(typeof token !== "undefined"){
        // Retrieves only the token and removes the "Bearer" prefix
        token = token.slice(7, token.length);

        return jwt.verify(token, secret, (error, data) => {
            if (error){

                return null;

            } else {

                // The "decode" method is used to obtain the information from the JWT
                // The "{complete:true}" option allows us to return additional information from the JWT token
                // Returns an object with access to the "payload" property which contains user information stored when the token was generated
                // The payload contains information provided in the "createAccessToken" method defined above (e.g. id, email and isAdmin)
                return jwt.decode(token, {complete:true}).payload;
            };

        });

    // Token does not exist
    } else {

        return null;

    };

};