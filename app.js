const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes')

dotenv.config();

const app = express();
const port = 2002;

// MongoDB Connection
mongoose.connect(process.env.MONGODB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection;
db.on('open', () => console.log('Connected to MongoDB!'));

app.use(
    cors(
        {
        origin: process.env.CLIENT_URL,
        credentials: true
    }
    ));
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.get("/api/test", (req, res) => {
    return res.status(200).send("API is working!");
  })

// Routes
app.use('/users', userRoutes);
app.use('/products', productRoutes);

app.listen(process.env.PORT || port, () => {
    console.log(`API is now running on localhost:${port}`);
});