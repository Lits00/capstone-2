const mongoose = require('mongoose');

const cart_schema = new mongoose.Schema({
    userId: {type: String},
    products: [
        {
        productId: {type: String},
        name: {type: String},
        price: {type: Number},
        quantity: {type: Number},
        subtotal: {type: Number}
        }
    ],
    total: {type: Number}

})

module.exports = mongoose.model('Carts', cart_schema);