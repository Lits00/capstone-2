const mongoose = require('mongoose');

const order_schema = new mongoose.Schema({
    userId: {
        type: String,
        required: [true, 'UserID is Required.']
    },
    products: [
        {
            productId: {
                type: String,
                required: [true, 'ProductID is Required.']
            },
            name: {
                type: String,
                required: [true, 'Product name is Required.']
            },  
            quantity: {
                type: Number,
                required: [true, 'Product quantity is Required.']
            }
        }
    ],
    totalAmount: {
        type: Number,
        default: 0
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model('Order', order_schema)