const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController');
const auth = require('../auth');
const { request } = require('express');

// User Registration
router.post("/register", (request, response) => {
    UserController.register(request.body).then((result) => {
        response.send(result)
    });
});

// Login User Authentication
router.post("/login", (request, response) => {
    UserController.login(request.body).then((result) => {
        response.send(result)
    });
});

// Get user details from token
router.get("/details", auth.verify, (request, response) => {

	// Retrieves the user data from the token
	const user_data = auth.decode(request.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	UserController.getProfile({userId : user_data.id}).then(result => response.send(result));

});

// User Checkout (Create Order)
router.post("/order-product", auth.verify, (request, response) => {
    let data = {
        info: request.body,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    UserController.orderProduct(data).then((result) => {
        response.send(result);
    });
});

// Retrieve user details
router.get("/:userId/details", (request, response) => {
    UserController.getUserDetails(request.params.userId).then((result) => {
        response.send(result)
    })
})

// [Stretch Goal]
// Set user as admin (Admin Only)
router.patch("/:userId/set-admin", auth.verify, (request, response) => {
    let data = {
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    UserController.setAsAdmin(request.params.userId, data).then((result) => {
        response.send(result)
    })
})

// Retrieve non-admin orders
router.get("/:userId/orders", auth.verify, (request, response) => {
    let data = {
        userId: request.params.userId,
    }
    UserController.getUserOrders(request.params.userId, data).then((result) => {
        response.send(result)
    })
})

// Retrieve authenticated user's orders
router.get("/:userId/order-list", auth.verify, (request, response) => {
    let data = {
        userId: request.params.userId,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    UserController.getUserOrders(request.params.userId, data).then((result) => {
        response.send(result)
    })
})

// Retrieve all orders (Admin)
router.get("/orders", auth.verify, (request, response) => {
    let data = {
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    UserController.getAllOrders(data).then((result) => {
        response.send(result)
    })
})

// Add to Cart
router.post("/cart", auth.verify, (request,response) => {
    let data = {
        info: request.body,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    UserController.addToCart(data).then((result) => {
        response.send(result)
    })
})

router.post("/cart/add-items", auth.verify, (request, response) => {
    let data = {
        info: request.body,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    UserController.addItems(data).then((result) => {
        response.send(result)
    })
})

module.exports = router;