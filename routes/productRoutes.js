const express = require('express');
const router = express.Router();
const ProductController = require('../controllers/ProductController');
const auth = require('../auth');
const { response } = require('express');

// Product creation
router.post("/create-product", auth.verify, (request, response) => {
    const data = {
        product: request.body,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    };

    ProductController.addProduct(data).then((result) => {
        response.send(result);
    });
});

// Retrieve all Products
router.get("/product-list", (request, response) => {
    ProductController.getAllProducts().then((result) => {
        response.send(result);
    })
})

// Retrieve all Active product(s)
router.get("/active-product", (request, response) => {
    ProductController.getAllActive().then((result) => {
        response.send(result);
    });
});

// Retrieve single product
router.get("/:productId", (request, response) => {
    ProductController.getProduct(request.params.productId).then((result) => {
        response.send(result);
    });
});

// Update product
router.patch("/:productId/update", auth.verify, (request, response) => {
    const data = {
        product: request.body,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    };

    ProductController.updateProduct(request.params.productId, data).then((result) => {
        response.send(result);
    });
});

// Archive product
router.patch("/:productId/archive", auth.verify, (request, response) => {
    const data = {
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    };

    ProductController.archiveProduct(request.params.productId, data).then((result) => {
        response.send(result);
    });
});

// Retrieve product
router.patch("/:productId/retrieve", auth.verify, (request, response) => {
    const data = {
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    };

    ProductController.retrieveProduct(request.params.productId, data).then((result) => {
        response.send(result);
    });
});

module.exports = router;